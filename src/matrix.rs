pub struct Matrix4x4 {
    pub data: [[f64; 4]; 4],
}

impl Matrix4x4 {
    pub fn zeros() -> Matrix4x4 {
        Matrix4x4 {
            data: [[0.0; 4]; 4],
        }
    }
    pub fn ones() -> Matrix4x4 {
        Matrix4x4 {
            data: [[1.0; 4]; 4],
        }
    }
    pub fn fill(v: f64) -> Matrix4x4 {
        Matrix4x4 {
            data: [[v; 4]; 4],
        }
    }
    pub fn identity() -> Matrix4x4 {
        Matrix4x4 {
            data: [[1.0, 0.0, 0.0, 0.0],
                   [0.0, 1.0, 0.0, 0.0],
                   [0.0, 0.0, 1.0, 0.0],
                   [0.0, 0.0, 0.0, 1.0]],
        }
    }
    pub fn dot(&self, other: &Matrix4x4) -> Matrix4x4 {
        let mut result = Matrix4x4::zeros();
        for i in 0..4 {
            for j in 0..4 {
                for k in 0..4 {
                    result.data[i][j] += self.data[i][k] * other.data[k][j];
                }
            }
        }
        result
    }
    pub fn dot_vectors(&self, other: &Vec<Vector4>) -> Vec<Vector4> {
        let mut result = Vec::new();
        for i in 0..other.len() {
            let mut vec = Vector4::zeros();
            for j in 0..4 {
                for k in 0..4 {
                    vec.data[j] += self.data[j][k] * other[i].data[k];
                }
            }
            result.push(vec);
        }
        result
    }
    pub fn transpose(&self) -> Matrix4x4 {
        let mut result = Matrix4x4::zeros();
        for i in 0..4 {
            for j in 0..4 {
                result.data[i][j] = self.data[j][i];
            }
        }
        result
    }
    pub fn translation_matrix(x: f64, y: f64, z: f64) -> Matrix4x4 {
        let mut t = Matrix4x4::identity();

        t.data[0][3] = x;
        t.data[1][3] = y;
        t.data[2][3] = z;

        return t;
    }

    pub fn scaling_matrix(x: f64, y: f64, z: f64) -> Matrix4x4 {
        let mut s = Matrix4x4::zeros();

        s.data[0][0] = x;
        s.data[1][1] = y;
        s.data[2][2] = z;
        s.data[3][3] = 1.0;

        return s;
    }

    pub fn rotation_matrix_x(angle: f64) -> Matrix4x4 {
        let mut r = Matrix4x4::zeros();

        r.data[0][0] = 1.0;
        r.data[1][1] = angle.cos();
        r.data[1][2] = angle.sin();
        r.data[2][1] = -angle.sin();  // or - angle.sin() ?
        r.data[2][2] = angle.cos();
        r.data[3][3] = 1.0;

        return r;
    }

    pub fn rotation_matrix_y(angle: f64) -> Matrix4x4 {
        let mut r = Matrix4x4::zeros();

        r.data[0][0] = angle.cos();
        r.data[0][2] = -angle.sin();
        r.data[1][1] = 1.0;
        r.data[2][0] = angle.sin();
        r.data[2][2] = angle.cos();
        r.data[3][3] = 1.0;

        return r;
    }

    pub fn rotation_matrix_z(angle: f64) -> Matrix4x4 {
        let mut r = Matrix4x4::zeros();

        r.data[0][0] = angle.cos();
        r.data[0][1] = angle.sin();
        r.data[1][0] = -angle.sin();
        r.data[1][1] = angle.cos();
        r.data[2][2] = 1.0;
        r.data[3][3] = 1.0;

        return r;
    }

    pub fn rotation_matrix(angle_x: f64, angle_y: f64, angle_z: f64) -> Matrix4x4 {
        let r_x = Matrix4x4::rotation_matrix_x(angle_x);
        let r_y = Matrix4x4::rotation_matrix_y(angle_y);
        let r_z = Matrix4x4::rotation_matrix_z(angle_z);

        return r_x.dot(&r_z).dot(&r_y);
    }

    pub fn projection_matrix(width: f64, height: f64, depth: f64) -> Matrix4x4 {
        let mut p = Matrix4x4::zeros();

        p.data[0][0] = width;
        p.data[1][1] = height;
        p.data[2][2] = 1.0;
        p.data[2][3] = 1.0 / depth;

        return p;
    }
}

pub struct Vector4 {
    pub data: [f64; 4],
}

impl Vector4 {
    pub fn new(x: f64, y: f64, z: f64, w: f64) -> Vector4 {
        Vector4 {
            data: [x, y, z, w],
        }
    }
    pub fn zeros() -> Vector4 {
        Vector4 {
            data: [0.0; 4],
        }
    }
    pub fn ones() -> Vector4 {
        Vector4 {
            data: [1.0; 4],
        }
    }
    pub fn fill(v: f64) -> Vector4 {
        Vector4 {
            data: [v; 4],
        }
    }
    pub fn norm(&self) -> f64 {
        let mut result = 0.0;
        for i in 0..4 {
            result += self.data[i] * self.data[i];
        }
        result
    }
    pub fn dot(&self, other: &Vector4) -> f64 {
        let mut result = 0.0;
        for i in 0..4 {
            result += self.data[i] * other.data[i];
        }
        result
    }
    pub fn cross(&self, other: &Vector4) -> Vector4 {
        Vector4 {
            data: [self.data[1] * other.data[2] - self.data[2] * other.data[1],
                   self.data[2] * other.data[0] - self.data[0] * other.data[2],
                   self.data[0] * other.data[1] - self.data[1] * other.data[0],
                   1.0],
        }
    }
}
