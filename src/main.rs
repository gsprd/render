mod matrix;

use matrix::{Matrix4x4, Vector4};
use std::{thread, time};

const WIDTH: i32 = 600;
const HEIGHT: i32 = 450;

fn main() -> Result<(), Box<dyn std::error::Error>> {

    // Object
    let mut cube = Vec::<Vector4>::new();

    cube.push(Vector4::new(0.0, 0.0, 0.0, 1.0));
    cube.push(Vector4::new(1.0, 0.0, 0.0, 1.0));
    cube.push(Vector4::new(0.0, 1.0, 0.0, 1.0));
    cube.push(Vector4::new(0.0, 0.0, 1.0, 1.0));
    cube.push(Vector4::new(1.0, 1.0, 0.0, 1.0));
    cube.push(Vector4::new(1.0, 0.0, 1.0, 1.0));
    cube.push(Vector4::new(0.0, 1.0, 1.0, 1.0));
    cube.push(Vector4::new(1.0, 1.0, 1.0, 1.0));

    // [-0.7, -0.3, -1.0, 1.0],
    // [ 0.7, -0.3, -1.0, 1.0],
    // [-0.7,  0.3, -1.0, 1.0],
    // [-0.7, -0.3,  1.0, 1.0],
    // [ 0.7,  0.3, -1.0, 1.0],
    // [ 0.7, -0.3,  1.0, 1.0],
    // [-0.7,  0.3,  1.0, 1.0],
    // [ 0.7,  0.3,  1.0, 1.0],

    // [ 0.0,  0.3,  0.5, 1.0],
    // [ 0.0,  0.3, -0.5, 1.0],
    // [ 0.5,  0.3,  0.0, 1.0],
    // [-0.5,  0.3,  0.0, 1.0],
    // [ 0.35,  0.3,  0.35, 1.0],
    // [-0.35,  0.3,  0.35, 1.0],
    // [ 0.35,  0.3, -0.35, 1.0],
    // [-0.35,  0.3, -0.35, 1.0],

    // [ 0.0,  0.7,  0.5, 1.0],
    // [ 0.0,  0.7, -0.5, 1.0],
    // [ 0.5,  0.7,  0.0, 1.0],
    // [-0.5,  0.7,  0.0, 1.0],
    // [ 0.35,  0.7,  0.35, 1.0],
    // [-0.35,  0.7,  0.35, 1.0],
    // [ 0.35,  0.7, -0.35, 1.0],
    // [-0.35,  0.7, -0.35, 1.0],

    // [-0.7, -0.3, -0.45, 1.0],
    // [-0.7,  0.3, -0.45, 1.0],
    // [-0.7, -0.3,  0.45, 1.0],
    // [-0.7,  0.3,  0.45, 1.0],

    // [-0.9, -0.3, -0.4, 1.0],
    // [-0.9,  0.3, -0.4, 1.0],
    // [-0.9, -0.3,  0.4, 1.0],
    // [-0.9,  0.3,  0.4, 1.0],

    // Initialize render
    let mut render = [[0; WIDTH as usize]; HEIGHT as usize];

    for theta in (0..(5 * 360)).step_by(4) {

        // Convert to radian
        let theta = (90.0 + theta as f64) * std::f64::consts::PI / 180.0;

        // Object transformation
        let object_translation = Matrix4x4::translation_matrix(-0.5, -0.5, -0.5);
        let object_rotation = Matrix4x4::rotation_matrix(theta, 0.0, 0.0);
        let object_scaling = Matrix4x4::scaling_matrix(3.0, 3.0, 3.0);
        let model_tranform = object_rotation.dot(&object_scaling.dot(&object_translation));

        // View transformation
        let camera_translation = Matrix4x4::translation_matrix(0.0, 0.0, 6.0);
        let camera_rotation = Matrix4x4::rotation_matrix(0.0, 0.0, 0.0);
        let view_transform = camera_rotation.dot(&camera_translation);

        // Projection transformation
        let width = 4.0/3.0;
        let height = 1.0;
        let depth = 2.0;
        let projection_transform = Matrix4x4::projection_matrix(width, height, depth);

        // Model view projection matrix
        let mvp_matrix =
            projection_transform.dot(&view_transform.dot(&model_tranform));

        // Transform object
        let mut cube_proj = mvp_matrix.dot_vectors(&cube);

        // Normalize homogeneous coordinates
        for point in cube_proj.iter_mut() {
            point.data[0] /= point.data[2];
            point.data[1] /= point.data[2];
        }

        // Prepare render
        for point in cube_proj.iter() {
            let i = HEIGHT as f64 / 2.0 * (1.0 + point.data[0]);
            let j = WIDTH as f64 / 2.0 * (1.0 - point.data[1]);
            render[i as usize][j as usize] = 1;
        }

        // Display render
        for i in 0..HEIGHT {
            for j in 0..WIDTH {
                if render[i as usize][j as usize] == 1 {
                    print!("\u{001b}[47m  \u{001b}[0m");
                    // print!("##");
                } else {
                    print!("  ");
                }
            }
            println!();
        }

        // Clean render
        for point in cube_proj.iter() {
            let i = HEIGHT as f64 / 2.0 * (1.0 + point.data[0]);
            let j = WIDTH as f64 / 2.0 * (1.0 - point.data[1]);
            render[i as usize][j as usize] = 0;
        }

        // Wait
        let delay = time::Duration::from_millis(60);
        thread::sleep(delay);

        // Clear screen
        println!("\u{001b}[2J");

    }

    Ok(())

}
